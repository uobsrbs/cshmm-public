
#include "defines.hpp"

#include "utils.h"

void zunpack( double *zeta, double **z) {
  // unpack vector zeta into symmetric matrix z
  int i, j, ind; 
  for( ind=i=0; i<2*(NFREQS); i++) for( j=i; j<2*(NFREQS); j++) 
    z[i][j] = z[j][i] = zeta[ind++];
}

void zpack( double **z, double *zeta) {
  // pack symmetric matrix z (only one triangle considered) into vector zeta
  int i, j, ind; 
  for( ind=i=0; i<2*(NFREQS); i++) for( j=i; j<2*(NFREQS); j++) 
    zeta[ind++] = z[i][j];
}
