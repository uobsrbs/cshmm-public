#include "memory.h"
#include <math.h>
double cholsqrt(double **,double **,int) ; 
void cholinv(double **,double **,int) ; 
void cholsqr(double **,double **,int) ; 

/* -------------------------------------------------------------------

   cholesky inverts a positive-definite symmetric matrix.
                                                                      */
double cholesky(double **a,double **b,int n)
{ double qdet = cholsqrt(a,b,n) ; 
  cholinv(b,b,n) ;
  cholsqr(b,b,n) ; 
  return qdet ; 
}
double cholesky(double **a,double **b) 
{ int n ; 
  insist(dimension0(a)==(n=dimension1(a)),orscream
   ("cholesky passed a non-square matrix.")) ; 
  insist(dimension0(b)==n&&dimension1(b)==n,orscream
   ("cholesky passed incompatible matrices.")) ; 
  return cholesky(a,b,n) ; 
}
/* -------------------------------------------------------------------

   cholsqrt looks at a[i][j] only for i<=j, and constructs b with  
   b[i][j]=0 for i>j. it may be run in place if you wish, ie. with a=b.
                                                                      */
double cholsqrt(double **a,double **b,int n)
{ int i,j,k ; 
  double q,qdet=1 ; 

  for(j=0;j<n;j++)
  { q = a[j][j] ; 
    for(i=0;i<j;i++) q -= b[i][j] * b[i][j] ; 
    if(q<=0) return 0.0 ; 
    qdet *= q ; 
    b[j][j] = sqrt(q) ;  
    for(k=j+1;k<n;k++)
    { q = 0 ; 
      for(i=0;i<j;i++) q += b[i][j] * b[i][k] ;
      b[j][k] = ( a[j][k] - q ) / b[j][j] ; 
    }
  }
  for(j=0;j<n;j++) for(k=0;k<j;k++) b[j][k] = 0 ; 
  return qdet ; 
}
/* -------------------------------------------------------------------

   cholinv looks at a[i][j] only for i<=j, and constructs b with b[i][j]
   zero for i<j. it may be run in place if you wish, ie. with a=b.
                                                                      */
void cholinv(double **a,double **b,int n)
{ int i,j,k ; 
  double q ; 

  for(j=0;j<n;j++)
  { b[j][j] = 1 / a[j][j] ; 
    for(i=j-1;i>=0;i--)
    { q = 0 ; 
      for(k=i;k<j;k++) q += b[k][i] * a[k][j] ;
      b[j][i] = - q * b[j][j] ; 
    }
  }
  for(j=0;j<n;j++) for(k=0;k<j;k++) b[k][j] = 0 ; 
}
/* -------------------------------------------------------------------

   cholsqr looks at a[i][j] only for i>=j, and constructs the complete 
   matrix b. it may be run in place if you wish, ie. with a=b.
                                                                      */
void cholsqr(double **a,double **b,int n)
{ int i,j,k ; 
  double q ; 

  for(i=0;i<n;i++) for(j=i;j<n;j++)
  { q = 0 ; 
    for(k=j;k<n;k++) q += a[k][i] * a[k][j] ;
    b[i][j] = q ; 
  }
  for(j=0;j<n;j++) for(k=0;k<j;k++) b[j][k] = b[k][j] ; 
}
