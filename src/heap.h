
/* header file to deal with a heap */

#include <string.h>

struct heapitem {
  void *ptr; 
  int len; 
  double scr; 
  heapitem() { // constructor
    ptr = 0; 
    scr = len = 0; 
  }
  heapitem(void *a, int b, double c) { // initializer
    ptr = a; 
    len = b; 
    scr = c; 
  } 
};
 
static void swap( struct heapitem &a, struct heapitem &b) { // exchanges heap item a and b
  heapitem c = b; 
  b = a; 
  a = c; 
} 

struct heap { 
  struct heapitem *list; 
  int nlist, listlen; 
  heap() { 
    nlist = listlen = -1; 
  } 
  void init(int n) { 
    listlen = n; 
    list = (struct heapitem *) calloc(n,sizeof(struct heapitem)); 
    nlist = 0; 
  } 

  void *add(void *buf, int nbuf, double x) {
    int i, j;
    void *p; 
    struct heapitem h; 
    if( listlen<0 ) {
      fprintf(stderr,"empty heap list\n"); 
      exit(EXIT_FAILURE); 
    }
    if( nlist==listlen&&x<=list[0].scr ) return 0; // if score is not good enough, don't add it on

    // make a copy of buf
    if( nlist==listlen&&list[0].len==nbuf ) p = list[0].ptr;
    else {
      if( nlist==listlen ) free(list[0].ptr);
      p = (void *) calloc(nbuf,1);
    }
    memcpy( p, buf, nbuf);     // copy nbuf bytes from buf -> p
    h = heapitem( p, nbuf, x); // and turn it into an item to go on the heap

    if( nlist<listlen ) {
      list[nlist++] = h;             // is this a tree-sort?
      for( j=nlist-1; j>0; j=i) {
	i = (j-1)/2;  
        if( x>=list[i].scr ) return list[j].ptr; 
        swap( list[i], list[j]); 
      }
      return p; 
    }
    for( i=0; 2*i+2<nlist; i=j) {
      j = 2*i + 1; 
      if( list[j+1].scr<list[j].scr ) j += 1; 
      if( x>list[j].scr ) { 
	list[i] = list[j]; 
	continue; 
      } 
      list[i] = heapitem(p,nbuf,x);
      return p; 
    }
    j = 2*i + 1 ; 
    if( j>=nlist||x<=list[j].scr ) list[i] = h;
    else { 
      list[i] = list[j]; 
      list[j] = h; 
    } 
    return p; 
  }

  int sort() {
    int i, j, n=nlist; 
    struct heapitem h; 
    for( nlist--; nlist>0; nlist--) {
      h = list[nlist]; 
      list[nlist] = list[0]; 
      for( i=0; 2*i+2<nlist; i=j) {
	j = 2*i + 1; 
        if( list[j+1].scr<list[j].scr ) j += 1; 
        if( h.scr>list[j].scr ) list[i] = list[j];  
        else { 
	  list[i] = h; 
	  break; 
	} 
      }
      j = 2*i + 1; 
      if( j>=nlist-1 ) {
	if( j>=nlist||h.scr<=list[j].scr ) list[i] = h; 
        else { 
	  list[i] = list[j]; 
	  list[j] = h; 
	}
      }
    }
    listlen = -1; 
    return n; 
  }

};
