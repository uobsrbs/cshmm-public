
#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <cstdlib>
#include <cmath>

template <typename T>
T runif( T maxVal = 1.0 ) {
  return maxVal * ( (T) rand() / RAND_MAX );
}

int randInt( int maxVal ) {
  return (int) ( maxVal * runif<float>() );
}

template <typename T>
T gaussian( T mean=0, T sd=1 ) {
  return mean + sd * sqrt( -2.0*log(runif<T>()) ) * cos( 2.0*M_PI*runif<T>() );
}

#endif // RANDOM_HPP
