
#include "defines.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <cmath>

#include "memory.h"
#include "matrix.h"
#include "heap.h"
#include <string.h>

#include "utils.h"

double cholesky( double **, double **);
void  printheap( heap *, int, int );

inline double ranf() { return (double) rand() / RAND_MAX; }
inline int rani(int iMax) { return (int) (iMax*ranf()); }
double gaussRand() {
	return sqrt(-2*log( ranf() )) * cos( 2.0*M_PI*ranf() );
}

int cmpHist( story a, story b ) {
	for( int ii=0; ii<TRIM; ++ii ) {
		if( a.hist[ii] < b.hist[ii] ) return -(ii+1);
		if( a.hist[ii] > b.hist[ii] ) return +(ii+1);
	}
	return 0;
}

int cmpHist( const void* a, const void* b ) {
	int ha, hb;
	for( int ii=0; ii<TRIM; ++ii ) {
		ha = (*(story *) ((*(heapitem *) a).ptr)).hist[ii];
		hb = (*(story *) ((*(heapitem *) b).ptr)).hist[ii];
		if( ha < hb ) return -(ii+1);
		if( ha > hb ) return +(ii+1);
	}
	return 0;
}

int sortHist( heap hp ) { 
	qsort( hp.list, hp.nlist, sizeof(heapitem), cmpHist ); 
	return hp.nlist;
}

int main( int argc, char* argv[] ) {

	int t_debug = -1;

	int last_ph = -1;

	int ii, jj, np, nf;

	std::ifstream synthFile;
	if( argc > 1 ) synthFile.open( argv[1] );
	else {
		std::cerr << "Usage: " << argv[0] 
			<< " <phone-data> <synth-data> [nsd=10.0 [fsd=10.0]]" << std::endl;
		exit(1);
	}

	int nph;
	double **Epcov = matrix(NFREQS, NFREQS), **Apcov = matrix(NFREQS, NFREQS),
		**Ep = matrix(NFREQS, NFREQS), **Ap = matrix(NFREQS, NFREQS);
	double logep, logap;

	// read in N_{\phi} and error std. devs
	synthFile >> nph;
	int dmin, dmax, tmin, tmax;
	// read in the dwell and transition time ranges
	synthFile >> dmin >> dmax >> tmin >> tmax;
	// read in the phonemes
	std::vector<phoneme> ph(nph);
	for( np=0; np<nph; ++np ) {
		synthFile >> ph.at(np).id;
		for( nf=0; nf<NFREQS; ++nf ) synthFile >> ph.at(np).f[nf];
	}
	synthFile.close();

	// read in the covariances
	double nsd = 10.0; if( argc > 3 ) nsd = atof( argv[3] );
	double fsd = 10.0; if( argc > 4 ) fsd = atof( argv[4] );

	double nvar = nsd*nsd, fvar = fsd*fsd;

	Epcov[0][0] = Epcov[1][1] = Epcov[2][2] = nvar;
	Epcov[0][1] = Epcov[0][2] = Epcov[1][2] = 0.0;

	Apcov[0][0] = Apcov[1][1] = Apcov[2][2] = fvar;
	Apcov[0][1] = Apcov[0][2] = Apcov[1][2] = 0.0;

	// symmetrise and convert to precision
	Epcov[1][0] = Epcov[0][1]; Epcov[2][0] = Epcov[0][2]; Epcov[2][1] = Epcov[1][2];
	Apcov[1][0] = Apcov[0][1]; Apcov[2][0] = Apcov[0][2]; Apcov[2][1] = Apcov[1][2];
	
	logep = -log( cholesky( Epcov, Ep ) );
	logap = -log( cholesky( Apcov, Ap ) );

	// synthFile now *really is* the synthetic data
	synthFile.open( argv[2] );

	int ph1, ph2;

	double y[NFREQS];
	int phonCheat;

	story hyp, hypSAVE;
	heap  ostk, nstk;

	int t, maxheap=250, hypno, nnstk, nostk, h;
	int cont_smh, flag, thresh=100, heapused=-1;
	double log2pi=log(2.0*M_PI);
	double mupmu, yey, qpdet, qbest, qq;
	double mup[2*NFREQS], mux[NFREQS], tmp[NFREQS];

	double **p = matrix( 2*NFREQS, 2*NFREQS ),
		**pinv   = matrix( 2*NFREQS, 2*NFREQS ),
		**r      = matrix( 2*NFREQS, 2*NFREQS );
	double **q = matrix(   NFREQS,   NFREQS ),
		**rxx    = matrix(   NFREQS,   NFREQS ),
		**rxs    = matrix(   NFREQS,   NFREQS );

	t = -1;

	double base_score;

	while( ! synthFile.eof() ) { // read through the file to the end
		t++;
		nstk.init( maxheap );

		for( nf=0; nf<NFREQS; ++nf ) synthFile >> y[nf];

		if( t==0 ) { // must initialise the system
			hyp.latest = t;

			for( ii=0; ii<2*NFREQS; ++ii ) for( jj=0; jj<2*NFREQS; ++jj ) p[ii][jj] = 0.0;
			for( ii=0; ii<NFREQS; ++ii ) { hyp.mu[ii] = y[ii]; hyp.mu[ii+NFREQS] = 0.0; }
			for( ii=0; ii<NFREQS; ++ii )
				for( jj=0; jj<NFREQS; ++jj ) p[ii][jj] = Ep[ii][jj];
			hyp.logk = 0.0;
			zpack( p, hyp.p );

			// set the history vector to 0 on the hypothesis
			for( ii=0; ii<LATENCY; ++ii ) hyp.hist[ii] = 0;

			hypSAVE = hyp; 

			// Must create a hypothesis for each of the possible initial phonemes
			for( np=0; np<nph; ++np ) {
				hyp = hypSAVE; 
				hyp.prevphon = np;
				// set the correct value in the history, >0 => dwell
				hyp.hist[0]  = np + 1;
				zunpack( hyp.p, p );
				for( ii=0; ii<NFREQS; ++ii ) mux[ii] = hyp.mu[ii];
				mprod( Ep, mux, mup ); mprod( Ap, ph.at(np).f, tmp );
				for( ii=0; ii<NFREQS; ++ii ) mup[ii] += tmp[ii];
				for( ii=0; ii<NFREQS; ++ii )
				for( jj=0; jj<NFREQS; ++jj ) rxx[ii][jj] = (p[ii][jj] += Ap[ii][jj]);
				cholesky( rxx, rxx );

				mprod( rxx, mup, hyp.mu );
				zpack( p, hyp.p );
	
				cholesky( Ap, rxx ); cholesky( Ep, rxs );
				for( ii=0; ii<NFREQS; ++ii )
				for( jj=0; jj<NFREQS; ++jj ) rxx[ii][jj] += rxs[ii][jj];
				qpdet = cholesky( rxx, rxx );
				for( ii=0; ii<NFREQS; ++ii ) tmp[ii] = mux[ii] - ph.at(np).f[ii];
				hyp.logk -= NFREQS*log2pi + log(qpdet) + qf(tmp, rxx, tmp); 

				nstk.add( &hyp, sizeof(story), hyp.logk );
			}

		} else for( hypno=0; hypno<nostk; ++hypno ) { // follow the trajectory

			cont_smh = 0;

			// take hypothesis off the heap
			hyp = ((story *) ostk.list[hypno].ptr)[0];
			base_score = hyp.logk;
			free( ostk.list[hypno].ptr );

			h = t - hyp.latest;
			memmove( &(hyp.hist[1]), hyp.hist, sizeof(int)*(LATENCY-1) );

			// hisory > 0 => dwell phase
			if( hyp.hist[0] > 0 ) { 
				np = hyp.hist[0] - 1;
				zunpack( hyp.p, p );
				for( ii=0; ii<NFREQS; ++ii ) for( jj=0; jj<NFREQS; ++jj ) rxs[ii][jj] = p[ii][jj];
				for( ii=0; ii<NFREQS; ++ii ) mux[ii] = hyp.mu[ii]; 
				for( ii=0; ii<NFREQS; ++ii )
				for( jj=0; jj<NFREQS; ++jj ) p[ii][jj] += Ep[ii][jj];
				zpack( p, hyp.p );

				for( ii=0; ii<NFREQS; ++ii ) 
				for( jj=0; jj<NFREQS; ++jj ) rxx[ii][jj] = p[ii][jj];
				cholesky( rxx, rxx );
				mprod( mux, rxs, tmp ); mprod( Ep, y, mup );
				for( ii=0; ii<NFREQS; ++ii ) tmp[ii] += mup[ii];
				for( ii=0; ii<NFREQS; ++ii ) hyp.mu[NFREQS+ii] = 0.0;
				mprod( rxx, tmp, hyp.mu );
				cholesky( rxs, rxs );
				cholesky( Ep, rxx );
				for( ii=0; ii<NFREQS; ++ii )
					for( jj=0; jj<NFREQS; ++jj ) rxs[ii][jj] += rxx[ii][jj];
				for( ii=0; ii<NFREQS; ++ii ) mux[ii] -= y[ii];
				qpdet = cholesky( rxs, rxs );

				hyp.logk += -NFREQS*log2pi - log(qpdet) - qf( mux, rxs, mux );
				if( h>=dmin && h<dmax ) hyp.logk += 2.0*log( (dmax-h) / (dmax-h+1.0) );
				if( h<dmax ) nstk.add( &hyp, sizeof(story), hyp.logk );
				if( h<dmin ) cont_smh = 1;
	
			} else { // transition phase

				if( h == 1 ) {
		
					zunpack( hyp.p, p );
					for( ii=0; ii<NFREQS; ++ii ) 
						for( jj=0; jj<NFREQS; ++jj ) {
							p[       ii][       jj] += Ep[ii][jj];
							p[NFREQS+ii][       jj]  = Ep[ii][jj];
							p[       ii][NFREQS+jj]  = Ep[ii][jj];
							p[NFREQS+ii][NFREQS+jj]  = Ep[ii][jj];
						}
					for( ii=0; ii<NFREQS; ++ii ) hyp.mu[NFREQS+ii] = y[ii] - hyp.mu[ii];
					zpack( p, hyp.p );

				} else {

					yey = qf( y, Ep, y ); 
					zunpack( hyp.p, p );

					qpdet = cholesky( p, pinv );
					hyp.logk += log(qpdet) + logep;

					mupmu = qf( hyp.mu, p, hyp.mu );
					mprod( hyp.mu, p, mup );
					mprod( Ep, y, tmp );
					for( ii=0; ii<NFREQS; ++ii ) {
						mup[ii] += tmp[ii]; mup[NFREQS+ii] += h * tmp[ii]; }
					for( ii=0; ii<NFREQS; ++ii ) 
						for( jj=0; jj<NFREQS; ++jj ) {
							p[       ii][       jj] += Ep[ii][jj]; 
							p[NFREQS+ii][NFREQS+jj] += h*h*Ep[ii][jj];
							p[       ii][NFREQS+jj] += h*Ep[ii][jj];
							p[NFREQS+ii][       jj] += h*Ep[ii][jj]; 
						}
					zpack( p, hyp.p );

					qpdet = cholesky( p, pinv );
					mprod( mup, pinv, hyp.mu );
					hyp.logk -= log(qpdet) + NFREQS*log2pi;
					hyp.logk -= yey + mupmu - qf( hyp.mu, p, hyp.mu );

				}

				if( h>=tmin && h<tmax ) hyp.logk += 2.0*log( (tmax-h) / (tmax-h+1.0) );
				if( h<tmax ) nstk.add( &hyp, sizeof(story), hyp.logk );
				if( h<tmin ) cont_smh = 1;

			}

			if( cont_smh == 1 ) continue;

			hyp.latest = t;

			// if we are in a dwell, add transition
			if( hyp.hist[0] > 0 ) {
				for( ii=0; ii<NFREQS; ++ii )
					p[ii][NFREQS+ii] = p[NFREQS+ii][ii] = p[NFREQS+ii][NFREQS+ii] = 0.0;
				zpack( p, hyp.p );
				hyp.hist[0] = -hyp.hist[1];
				hyp.logk -= 2.0*log(dmax-h+1.0);
				nstk.add( &hyp, sizeof(story), hyp.logk );
	
			} else {

				for( ii=0; ii<NFREQS; ++ii ) 
				for( jj=0; jj<NFREQS; ++jj )
					q[ii][jj] = h*h*p[ii][jj] + p[NFREQS+ii][NFREQS+jj] -h*( p[NFREQS+ii][jj] + p[ii][NFREQS+jj] );
				cholesky( q, q );

				for( ii=0; ii<NFREQS; ++ii ) for( jj=0; jj<NFREQS; ++jj ) {
					r[ii][jj] = h*h*q[ii][jj];
					r[NFREQS+ii][jj] = ( r[ii][NFREQS+jj] = -h*q[ii][jj] );
					r[NFREQS+ii][NFREQS+jj] = q[ii][jj]; 
				}

				mprod( p, r, p, r ); msub( p, r, r );
				for( ii=0; ii<NFREQS; ++ii ) for( jj=0; jj<NFREQS; ++jj ) 
					p[ii][jj]  = r[ii][jj];
				for( ii=0; ii<NFREQS; ++ii ) p[NFREQS+ii][NFREQS+ii] = 0.0;
				zpack( p, hyp.p );
				hyp.logk -= 2.0*log(tmax-h+1.0);
				for( ii=0; ii<NFREQS; ++ii ) {
					hyp.mu[ii] += h*hyp.mu[NFREQS+ii]; 
					hyp.mu[NFREQS+ii] = 0.0; 
				}
	
				hypSAVE = hyp;
				for( np=0; np<nph; ++np ) {

					if( np == hypSAVE.prevphon ) continue; // no stutters
		
					hyp = hypSAVE;
					hyp.hist[0] = np + 1;
					zunpack( hyp.p, p );
					for( ii=0; ii<NFREQS; ++ii ) 
						for( jj=0; jj<NFREQS; ++jj ) rxs[ii][jj] = p[ii][jj];       
					for( ii=0; ii<NFREQS; ++ii ) mux[ii] = hyp.mu[ii];    
					for( ii=0; ii<NFREQS; ++ii )
						for( jj=0; jj<NFREQS; ++jj ) rxx[ii][jj] = ( p[ii][jj] += Ap[ii][jj] );
					zpack( p, hyp.p );              

					mprod( mux, rxs, tmp );
					mprod( Ap, ph.at(np).f, mup );
					for( ii=0; ii<NFREQS; ++ii ) tmp[ii] += mup[ii];
					cholesky( rxx, rxx );
					mprod( tmp, rxx, hyp.mu );
					cholesky( rxs, rxs );
					cholesky( Ap, rxx );
					for( ii=0; ii<NFREQS; ++ii ) 
					for( jj=0; jj<NFREQS; ++jj ) rxs[ii][jj] += rxx[ii][jj];
					for( ii=0; ii<NFREQS; ++ii ) mux[ii] -= ph.at(np).f[ii];
					qpdet = cholesky( rxs, rxs );                 

					// apply the mono-gram language model
					hyp.logk -= 2.0*log(nph);
					hyp.prevphon = np;

					hyp.logk += -NFREQS*log2pi - log( qpdet ) - qf( mux, rxs, mux );
					nstk.add( &hyp, sizeof(story), hyp.logk);
					
					// account for the possibility of zero-length dwells
					if( dmin == 0 ) {
						hyp.latest = t;

						for( ii=0; ii<NFREQS; ++ii ) {
							p[NFREQS+ii][ii] = p[ii][NFREQS+ii] = 0.0;
							p[NFREQS+ii][NFREQS+ii] = 0.0;
							hyp.mu[NFREQS+ii] = 0.0;
						}
						zpack( p, hyp.p );
						hyp.hist[0] = -hyp.hist[0];
						hyp.logk -= 2.0*log(dmax+1.0);
						nstk.add( &hyp, sizeof(story), hyp.logk );
					}
					
				}

		}

	}
		
	if(t) free(ostk.list);

#ifdef DYNAMIC // add in dynamic programming
	if( t > TRIM ) {
		ostk.init( sortHist(nstk) );
		hypSAVE = *(story *) nstk.list[0].ptr;
		for( ii=1; ii<nstk.nlist; ++ii ) {
			hyp = *(story *) nstk.list[ii].ptr;
			if( cmpHist( hyp, hypSAVE ) == 0 ) { 
				if( hyp.logk > hypSAVE.logk ) hypSAVE = hyp;
			} else { 
				ostk.add( &hypSAVE, sizeof(story), hypSAVE.logk );
				hypSAVE = hyp;
			}
		}
		ostk.add( &hypSAVE, sizeof(story), hypSAVE.logk );
		nstk = ostk;
	}
#endif

	nnstk = nstk.sort();
	ostk.init(nnstk);

	nostk = 0;
	for( ii=0; ii<nnstk; ++ii ) {
		hyp = ((story *) nstk.list[ii].ptr)[0];
		if( ii==0 ) {
			qbest = hyp.logk;
			ph1 = hyp.hist[LATENCY-1];
			ph2 = hyp.hist[LATENCY-2];

			if( t >= LATENCY-1 && abs(ph1) != last_ph )
				std::cout << ph.at( (last_ph=abs(ph1))-1 ).id << " ";
		}
		hyp.logk -= qbest;
		if( hyp.logk > -2.0*thresh && ( t<LATENCY-1 || abs(hyp.hist[LATENCY-1])==abs(ph1) ) ) {
			ostk.add( &hyp, sizeof(story), hyp.logk );
			nostk += 1;
		}
		free( nstk.list[ii].ptr );
	}
	free( nstk.list );
	nostk = ostk.sort();

	if( nostk > heapused ) heapused = nostk;

#ifdef SHOWSTACK
	std::cerr << "### " << nostk << " ###" << std::endl;
	for( ii=0; ii<( nostk < 20 ? nostk : 20 ); ++ii ) {
		for( jj=0; jj<LATENCY; ++jj ) 
		fprintf( stderr, "%3d", ((story *) ostk.list[ii].ptr)[0].hist[jj] );
		std::cerr << std::setw(16) << std::setprecision(10) << ostk.list[ii].scr << " " << std::endl;
	}
#endif

	}

	bool success = false;
	for( hypno=0; hypno<nostk; ++hypno ) {
		hyp = ((story *) ostk.list[hypno].ptr)[0];
		if( hyp.hist[0] < 0 || t - hyp.latest > dmin ) { success = true; break; }
	}
	if( !success ) hyp = ((story *) ostk.list[0].ptr)[0];
	
	for( ii=LATENCY-2; ii>0; ii-- ) 
	if( t-1 >= ii && abs(hyp.hist[ii]) != last_ph )
		std::cout << ph.at( (last_ph=abs(hyp.hist[ii]))-1 ).id << " ";
	std::cout << std::endl;

	std::cerr << "## MAX HEAP USED: " << heapused << std::endl;

	return 0;
}
