
#include <string>

struct story {
  int latest, prevphon, nextphon, tscale; 
  int hist[LATENCY];

#ifdef PLOTTING
  double mu_hist[LATENCY * NFREQS];
#endif

  double mu[2*(NFREQS)], p[(NFREQS)*(2*(NFREQS)+1)], logpdet, logk; 
  // logk is actually 2*log(k)
  void print( int t);
};

// unpack vector zeta into symmetric matrix z
void zunpack( double *zeta, double **z); 
// pack symmetric matrix z (only one triangle considered) into zeta
void zpack( double **z, double *zeta);

void shift_left(  struct story &hyp, const int places ); 
void shift_right( struct story &hyp, const int places );

struct phoneme {
  double f[NFREQS];
  int id;
  std::string name;

  double mus0[NFREQS], pss0[NFREQS];

};
