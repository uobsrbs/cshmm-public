
#ifndef MEMORY_H
#define MEMORY_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#ifndef MATRIX_H
#define MATRIX_H

/* ----------------- define insist(...,orscream(...)) --------------- */

#define insist(a,b) \
((a)?0:(fprintf(stdout,"Fatal error at line %d of %s\n",\
        __LINE__,__FILE__),(b)))

static int orscream(const char *m,...) 
{ va_list vl ; 
  va_start(vl,m) ; 
  vfprintf(stderr,m,vl) ; 
  va_end(vl) ; 
  fprintf(stderr,"\n") ; 
  fflush(0) ; 
  exit(EXIT_FAILURE) ; 
} ;
/* ---------------------- define variadic free() -------------------- */

static void free(void *a,void *b) { free(a) ; free(b) ; } 
static void free(void *a,void *b,void *c) 
{ free(a) ; free(b) ; free(c) ; } 
static void free(void *a,void *b,void *c,void *d) 
{ free(a) ; free(b) ; free(c) ; free(d) ; } 
static void free(void *a,void *b,void *c,void *d,void *e) 
{ free(a) ; free(b) ; free(c) ; free(d) ; free(e) ; } 
static void free(void *a,void *b,void *c,void *d,void *e,void *f) 
{ free(a) ; free(b) ; free(c) ; free(d) ; free(e) ; free(f) ; } 

/* -------------- duplicate matrix.h (now redundant) ---------------- */

static void *cjcalloc(size_t a,size_t b)
{ insist((int)a>=0&&(int)b>=0,
         orscream("negative length %d requested.",a*b)) ; 
  void *p=calloc(a,b) ; 
  insist(p,orscream("unable to allocate %d bytes of memory.",b)) ; 
  return p ; 
} 
static void *cjcrealloc(void *a,size_t b)
{ insist((int)b>=0,orscream("negative length %d requested.",b)) ; 
  void *p=realloc(a,b) ; 
  if(b) insist(p,orscream("unable to reallocate %x to %d bytes.",a,b)) ;
  return p ; 
} 

/* vector allocators - free by calling 'free' */
// -- vector
static double *vector(int n) 
{ return (double *) cjcalloc(n,sizeof(double)) ; } 
static double *vector(double *a,int n) 
{ return (double *) cjcrealloc(a,n*sizeof(double)) ; } 

// -- ivector
static int *ivector(int n) 
{ return (int *) cjcalloc(n,sizeof(int)) ; } 
static int *ivector(int *a,int n) 
{ return (int *) cjcrealloc(a,n*sizeof(int)) ; } 

// -- charvector
static char *charvector(int n) 
{ return (char *) cjcalloc(n,sizeof(char)) ; } 
static char *charvector(char *c) 
{ int i ; 
  char *ret ; 
  for(i=0;c[i];i++) ; 
  ret = charvector(1+i) ;
  for(;i>=0;i--) ret[i] = c[i] ; 
  return ret ; 
} 
static char *charvector(char *a,int n) 
{ return (char *) cjcrealloc(a,n*sizeof(char)) ; } 

// -- strvector
static char **strvector(int n) 
{ return (char **) cjcalloc(n,sizeof(char*)) ; } 
static char **strvector(char **a,int n) 
{ return (char **) cjcrealloc(a,n*sizeof(char*)) ; } 

// -- shortvector
static short *shortvector(int n) 
{ return (short *) cjcalloc(n,sizeof(short)) ; } 
static short *shortvector(short *a,int n) 
{ return (short *) cjcrealloc(a,n*sizeof(short)) ; } 

/* 2- and 3- dimensional matrices of doubles */
static double **matrix(int m,int n)
{ int i ; 
  double **a = (double **) cjcalloc(m,sizeof(double *)) ;
  a[0] = (double *) cjcalloc(2+m*n,sizeof(double)) ; 
  ((int *)a[0])[0] = m ; 
  ((int *)a[0])[1] = n ; 
  a[0] += 2 ; 
  for(i=1;i<m;i++) a[i] = a[i-1] + n ; 
  return a ; 
}  
static int dimension0(double **a) { return ((int *)(a[0]-2))[0] ; } 
static int dimension1(double **a) { return ((int *)(a[0]-2))[1] ; } 

static double ***matrix(int m,int n,int l)
{ int i ; 
  double ***a = (double ***) cjcalloc(m,sizeof(double **)) ;
  a[0] = (double **) cjcalloc(m*n,sizeof(double *)) ; 
  for(i=1;i<m;i++) a[i] = a[i-1] + n ; 
  a[0][0] = (double *) cjcalloc(m*n*l,sizeof(double)) ; 
  for(i=1;i<m*n;i++) a[0][i] = a[0][i-1] + l ; 
  return a ; 
}  
/* free them */
static void freematrix(double **a) { if(a) free(a[0]-2,a) ; } 
static void freematrix(double **a,double **b)
{ freematrix(a) ; freematrix(b) ; } 
static void freematrix(double **a,double **b,double **c)
{ freematrix(a) ; freematrix(b) ; freematrix(c) ; } 
static void freematrix(double **a,double **b,double **c,double **d)
{ freematrix(a) ; freematrix(b) ; freematrix(c) ; freematrix(d) ; } 

static void freematrix(double **a,double **b,double **c,double **d,
                       double **e)
{ freematrix(a) ; freematrix(b) ; freematrix(c) ; freematrix(d) ; 
  freematrix(e) ;
} 

static void freematrix(double **a,double **b,double **c,double **d,
                       double **e,double **f)
{ freematrix(a) ; freematrix(b) ; freematrix(c) ; freematrix(d) ; 
  freematrix(e) ; freematrix(f) ;
} 
static void freematrix(double ***a) { if(a) free(a[0][0],a[0],a) ; } 

/* 2- and 3- dimensional matrices of ints */
static int **imatrix(int m,int n)
{ int **a = (int **) cjcalloc(m,sizeof(int *)) ;
  a[0] = ivector(m*n) ; 
  for(int i=1;i<m;i++) a[i] = a[i-1] + n ; 
  return a ; 
}  
static int ***imatrix(int m,int n,int l)
{ int i ; 
  int ***a = (int ***) cjcalloc(m,sizeof(int **)) ;
  a[0] = (int **) cjcalloc(m*n,sizeof(int *)) ; 
  for(i=1;i<m;i++) a[i] = a[i-1] + n ; 
  a[0][0] = (int *) cjcalloc(m*n*l,sizeof(int)) ; 
  for(i=1;i<m*n;i++) a[0][i] = a[0][i-1] + l ; 
  return a ; 
}  
/* free them */
static void freeimatrix(int **a) { if(a) free(a[0],a) ; } 
static void freeimatrix(int ***a) { if(a) free(a[0][0],a[0],a) ; } 

/* swaps */
static void swap(int &a,int &b) { int c=b ; b = a ; a = c ; } 
static void swap(double &a,double &b) { double c=b ; b = a ; a = c ; }
static void swap(char &a,char &b) { char c=b ; b = a ; a = c ; } 
static void swap(int &a,double &b) { int c=(int) b ; b = a ; a = c ; }
static void swap(double &a,int &b) { int c=b ; b = (int) a ; a = c ; }

#endif

/* --- extend matrix.h to complexes if unreal.h already included ---- */

#ifdef UNREAL_H
/* allocate vectors and matrices of complexes */
static complex *cvector(int n) 
{ return (complex *) cjcalloc(n,sizeof(complex)) ; } 

static complex **cmatrix(int m,int n)
{ int i ; 
  complex **a = (complex **) cjcalloc(m,sizeof(complex *)) ;
  a[0] = (complex *) cjcalloc(m*n,sizeof(complex)) ; 
  for(i=1;i<m;i++) a[i] = a[i-1] + n ; 
  return a ; 
}  
static complex ***cmatrix(int m,int n,int l)
{ int i ; 
  complex ***a = (complex ***) cjcalloc(m,sizeof(complex **)) ;
  a[0] = (complex **) cjcalloc(m*n,sizeof(complex *)) ; 
  for(i=1;i<m;i++) a[i] = a[i-1] + n ; 
  a[0][0] = (complex *) cjcalloc(m*n*l,sizeof(complex)) ; 
  for(i=1;i<m*n;i++) a[0][i] = a[0][i-1] + l ; 
  return a ; 
}  
/* free them */
static void freecmatrix(complex **a) { if(a) free(a[0],a) ; } 
static void freecmatrix(complex ***a) { if(a) free(a[0][0],a[0],a) ; } 

#endif

/* ---------------------- define robust fopens --------------------- */

static FILE *fopenread(char *name)
{ FILE *f ; 
  if(name[0]=='-'&&name[1]=='-'&&name[2]==0) f = stdin ; 
  else f = fopen(name,"r") ; 
  insist(f,(perror(0),orscream
    ("Your input file %s could not be found.\n",name))) ;
  return f ; 
} 
static FILE *fopenwrite(char *name)
{ FILE *f ; 
  if(name[0]=='-'&&name[1]=='-'&&name[2]==0) f = stdout ; 
  else f = fopen(name,"w") ; 
  insist(f,(perror(0),orscream
    ("Unable to write to your file %s.\n",name))) ; 
  return f ; 
} 
static FILE *fopenappend(char *name)
{ FILE *f ;
 if(name[0]=='-'&&name[1]=='-'&&name[2]==0) f = stdout ;
 else f = fopen(name,"a") ;
 insist(f,(perror(0),orscream
   ("Unable to append to your file %s.\n",name))) ;
 return f ;
}
static FILE *fopenreadb(char *name)
{ FILE *f ; 
  if(name[0]=='-'&&name[1]=='-'&&name[2]==0) f = stdin ; 
  else f = fopen(name,"rb") ; 
  insist(f,(perror(0),orscream
    ("Your input file %s could not be found.\n",name))) ;
  return f ; 
} 
static FILE *fopenwriteb(char *name)
{ FILE *f ; 
  if(name[0]=='-'&&name[1]=='-'&&name[2]==0) f = stdout ; 
  else f = fopen(name,"wb") ; 
  insist(f,(perror(0),orscream
    ("Unable to write to your file %s.\n",name))) ; 
  return f ; 
} 
static FILE *fopenappendb(char *name)
{ FILE *f ;
 if(name[0]=='-'&&name[1]=='-'&&name[2]==0) f = stdout ;
 else f = fopen(name,"ab") ;
 insist(f,(perror(0),orscream
   ("Unable to append to your file %s.\n",name))) ;
  return f ; 
}
static char *freadline(FILE *ifl)
{ char *s=0 ; 
  int slen,ns,c,i ; 
  for(slen=ns=0;;)
  { c = fgetc(ifl) ; 
    if(c==EOF||c=='\n') 
    { if(slen>ns+1) s = (char *) cjcrealloc(s,ns+1) ; return s ; }
    if(ns>=slen-1)
    { slen += 10 + slen/2 ; 
      s = (char *) cjcrealloc(s,slen) ; 
      for(i=ns;i<slen;i++) s[i] = 0 ; 
    }
    s[ns++] = (char) c ; 
  }
}
static char *readline() { return freadline(stdin) ; } 
#endif
