
#include "defines.hpp"

#ifndef PHONEME_HPP
#define PHONEME_HPP

#include "random.hpp"

#include <cstring>
#include <algorithm>
#include <vector>

template <typename T, int NF=NFREQS>
class Phoneme {

public:
  Phoneme( int idn=-1, std::string ph_name="" ) {
    _id = idn;
    _name = ph_name;
    f.resize(NF);
    genFreqs();
  }

  int  id() const { return _id; }
  int& id() { return _id; }

  std::vector<T>  fq() const { return f; }
  std::vector<T>& fq() { return f; }

  T  fq(size_t i) const { return f.at(i); }
  T& fq(size_t i) { return f.at(i); }

  friend std::ostream& operator<< ( std::ostream &out, Phoneme const &p ) {
    out << p._id << " ";
    for( int i=0; i<p.f.size(); ++i ) out << p.f.at(i) << " ";
    out << std::endl;
    return out;
  }

private:
  int _id;
  std::string _name;
  std::vector<T> f;

  void genFreqs() {
    bool success = false;
    while( !success ) {
      success = true;
      for( int i=0; i<f.size(); ++i ) f.at(i) = 200.0 + runif(3000.0);
      std::sort( f.begin(), f.end() );
      for( int i=1; i<f.size(); ++i )
	if( f.at(i) < f.at(i-1) + 150.0 ) success = false;
    }
  }

};

#endif // PHONEME_HPP
