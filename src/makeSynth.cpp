
#include <iostream>
#include <fstream>

#include <cstdlib>
#include <ctime>

#include "random.hpp"
#include "phoneme.hpp"
#include "defines.hpp"

int main( int argc, char* argv[] ) {

	if( argc == 1 ) {
		std::cerr << "USAGE: " << argv[0] << " <length> [seed=0 [nsd=10.0 [fsd=10.0]]]" << std::endl;
		return -1;
	}

  //srand( time(NULL) );

  int dataLen = atoi( argv[1] );
  
  int seed = 10;
  if( argc > 2 ) seed += atoi( argv[2] );
  srand( seed );

  float nsd = 10.0, fsd = 10.0;
  if( argc > 3 ) nsd = atof( argv[3] );
  if( argc > 4 ) fsd = atof( argv[4] );

  // read in the phoneme inventory
  std::ifstream infile( "phonemes.dat" );
  int nph; infile >> nph;
  int dmin, dmax, tmin, tmax; infile >> dmin >> dmax >> tmin >> tmax;
  std::vector< Phoneme<float> > inventory(nph);
  for( int i=0; i<inventory.size(); ++i ) {
    Phoneme<float> p;
    infile >> p.id(); for( size_t j=0; j<NFREQS; ++j ) infile >> p.fq(j);
    inventory.at(i) = p;
  }
  infile.close();

  // generate the truth data
  std::vector<int> truth(dataLen+1); // (one extra for final transition)
  for( size_t i=0; i<truth.size(); ++i ) {
  	truth.at(i) = randInt(nph);
  	if( i>0 ) 
  		while( truth.at(i) == truth.at(i-1) ) truth.at(i) = randInt(nph);
  }

  std::ofstream truFile( "truth.dat" );
  for( size_t i=0; i<truth.size()-1; ++i )
  	truFile << truth.at(i) << " "; truFile << std::endl;
  truFile.close();

  // generate the dwell times
  std::vector<int> dwell(dataLen);
  for( size_t i=0; i<dwell.size(); ++i ) dwell.at(i) = dmin + randInt( dmax - dmin );
  
  // generate the transition times
  std::vector<int> trans(dataLen);
  for( size_t i=0; i<trans.size(); ++i ) trans.at(i) = tmin + randInt( tmax - tmin );

  std::ofstream timFile( "timing.dat" );
  int simt = 0;
  for( size_t i=0; i<dwell.size(); ++i ) {
    timFile << simt << " " << simt + dwell.at(i) << " " << truth.at(i) << std::endl;
    simt += dwell.at(i) + trans.at(i);
  }
  timFile.close();

  // initialise the dwell frequencies
  std::vector<float> freq(NFREQS), newFreq(NFREQS);
  for( size_t i=0; i<NFREQS; ++i ) 
    freq.at(i) = gaussian( inventory.at(truth[0]).fq(i), fsd );

  std::ofstream synFile( "synth.dat" );

  // begin to generate data
  for( int q=0; q<dataLen; ++q ) {
    
    // dwell first
    for( int h=0; h<dwell.at(q); ++h ) {
      for( size_t i=0; i<NFREQS; ++i ) 
				synFile << gaussian( freq.at(i), nsd ) << " "; synFile << std::endl;
    }
    
    // generate the next dwell frequency
    for( size_t i=0; i<NFREQS; ++i ) 
      newFreq.at(i) = gaussian( inventory.at(truth[q+1]).fq(i), fsd );

    // transition data
    for( int h=0; h<trans.at(q); ++h ) {
      float hh = (float) h / trans.at(q);
      for( size_t i=0; i<NFREQS; ++i ) 
				synFile << gaussian( (1-hh)*freq.at(i) + hh*newFreq.at(i), nsd ) << " ";
      synFile << std::endl;
    }

    freq = newFreq;

  }

  synFile.close();

  return 0;
}
