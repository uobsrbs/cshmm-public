static void madd(double **a,double **b,double **c)
{ int i,j,ni=dimension0(a),nj=dimension1(a) ;
  insist(ni==dimension0(b)&&ni==dimension0(c),orscream
    ("inconsistent first dimension in matrix add: "
     "%d/%d/%d",ni,dimension0(b),dimension0(c))) ; 
  insist(nj==dimension1(b)&&nj==dimension1(c),orscream
    ("inconsistent second dimension in matrix add: "
     "%d/%d/%d",nj,dimension1(b),dimension1(c))) ; 
  for(i=0;i<ni;i++) for(j=0;j<nj;j++) c[i][j] = a[i][j] + b[i][j] ; 
}
static void msub(double **a,double **b,double **c)
{ int i,j,ni=dimension0(a),nj=dimension1(a) ;
  insist(ni==dimension0(b)&&ni==dimension0(c),orscream
    ("inconsistent first dimension in matrix subtraction: "
     "%d/%d/%d",ni,dimension0(b),dimension0(c))) ; 
  insist(nj==dimension1(b)&&nj==dimension1(c),orscream
    ("inconsistent second dimension in matrix subtraction: "
     "%d/%d/%d",nj,dimension1(b),dimension1(c))) ; 
  for(i=0;i<ni;i++) for(j=0;j<nj;j++) c[i][j] = a[i][j] - b[i][j] ; 
}
static void mprod(double **a,double **b,double **c)
{ int i,j,k,ni=dimension0(a),nj=dimension1(a),nk=dimension1(b) ;
  insist(nj==dimension0(b),orscream
    ("inconsistent dimensions of matrices being multiplied: "
     "%dx%d times %dx%d",ni,nj,dimension0(b),nk)) ; 
  insist(ni==dimension0(c)&&nk==dimension1(c),orscream
    ("illegal dimensions of matrix product: "
     "%dx%d times %dx%d giving %dx%d",ni,nj,nj,nk,
                           dimension0(c),dimension1(c))) ; 
  double **z=matrix(ni,nk),q ; 
  for(i=0;i<ni;i++) for(k=0;k<nk;k++) 
  { for(q=j=0;j<nj;j++) q += a[i][j] * b[j][k] ; z[i][k] = q ; } 
  for(i=0;i<ni;i++) for(k=0;k<nk;k++) c[i][k] = z[i][k] ; 
  freematrix(z) ; 
}
static void mprod(double **a,double **b,double **c,double **d)
{ int i,j,k,ni=dimension0(a),nj=dimension1(a),nk=dimension1(b) ;
  insist(nj==dimension0(b),orscream
    ("inconsistent dimensions of matrices being multiplied: "
     "%dx%d times %dx%d",ni,nj,dimension0(b),nk)) ; 
  double **u=matrix(ni,nk),**v,q ; 
  for(i=0;i<ni;i++) for(k=0;k<nk;k++) 
  { for(q=j=0;j<nj;j++) q += a[i][j] * b[j][k] ; u[i][k] = q ; } 
  nj = nk ; 
  nk = dimension1(c) ; 
  insist(nj==dimension0(c),orscream
    ("inconsistent dimensions of matrices being multiplied: "
     "%dx%d times %dx%d",ni,nj,dimension0(c),nk)) ; 
  v = matrix(ni,nk) ; 
  for(i=0;i<ni;i++) for(k=0;k<nk;k++) 
  { for(q=j=0;j<nj;j++) q += u[i][j] * c[j][k] ; v[i][k] = q ; } 
  for(i=0;i<ni;i++) for(k=0;k<nk;k++) d[i][k] = v[i][k] ; 
  freematrix(u) ; freematrix(v) ;
}
static void mprod(double **a,double *b,double *c)
{ int i,j,ni=dimension0(a),nj=dimension1(a) ; 
  double *z=vector(ni),q ; 
  for(i=0;i<ni;i++) 
  { for(q=j=0;j<nj;j++) q += a[i][j] * b[j] ; z[i] = q ; } 
  for(i=0;i<ni;i++) c[i] = z[i] ; 
  free(z) ; 
} 
static void mprod(double *a,double **b,double *c)
{ int i,j,ni=dimension0(b),nj=dimension1(b) ; 
  double *z=vector(nj),q ; 
  for(j=0;j<nj;j++) 
  { for(q=i=0;i<ni;i++) q += a[i] * b[i][j] ; z[j] = q ; } 
  for(j=0;j<nj;j++) c[j] = z[j] ; 
  free(z) ; 
} 
static double iprod(double *a,double *b,int n)
{ int i ; 
  double q ; 
  for(q=i=0;i<n;i++) q += a[i] * b[i] ; 
  return q ; 
}
static double qf(double *a,double **b,double *c)
{ int i,j,ni=dimension0(b),nj=dimension1(b) ; 
  double q ; 
  for(q=i=0;i<ni;i++) for(j=0;j<nj;j++) q += a[i] * b[i][j] * c[j] ; 
  return q ; 
}
/*
main()
{ int i,j ; 
  double **a=matrix(3,4),b[3]={3,4,5},c[4]={4,5,6,7},ba[4],ac[3] ; 
  for(i=0;i<3;i++) for(j=0;j<4;j++) a[i][j] = 10*i + j ; 
  mprod(b,a,ba) ; 
  for(i=0;i<4;i++) printf("%4d ",(int) ba[i]) ; 
  printf("\n") ; 
  mprod(a,c,ac) ; 
  for(i=0;i<3;i++) printf("%4d ",(int) ac[i]) ; 
  printf("\n%d\n",(int) qf(b,a,c)) ; 
}
*/
