
#include <iostream>
#include <ctime>

#include "random.hpp"
#include "phoneme.hpp"

int main( int argc, char* argv[] ) {

  //srand( time(NULL) );

  int nph  = 40;
  if( argc > 1 ) nph = atoi( argv[1] );
  if( argc > 2 ) srand( 10 + atoi(argv[2] ) ); else srand( time(NULL) );
  int dmin = randInt(3), dmax = dmin + 2 + randInt(7),
    tmin = 2 + randInt(3), tmax = tmin + 2 + randInt(7);
  
  dmin = 0; dmax = 5; tmin = 2; tmax = 7;

  std::cout << nph << std::endl;
  std::cout << dmin << " " << dmax << " " << tmin << " " << tmax << std::endl;

  std::vector< Phoneme<float> > inventory(nph);

  for( int i=0; i<inventory.size(); ++i ) {
    Phoneme<float> p(i); inventory.at(i) = p;
  }

  for( int i=0; i<inventory.size(); ++i ) std::cout << inventory.at(i);

  return 0;
}
