#!/usr/bin/env python

import sys, os

if len(sys.argv) < 3:
  print "Usage: "+sys.argv[0]+" <reference> <hypothesis>"
  sys.exit(1)

try:
  refFile = open( sys.argv[1], "r" )
  refOut  = open( sys.argv[1]+".stm", "w" )
except:
  print "ERROR: Unable to open "+sys.argv[1]
  sys.exit(2)

try:
  hypFile = open( sys.argv[2], "r" )
  hypOut  = open( sys.argv[2]+".ctm", "w" )
except:
  print	"ERROR:	Unable to open "+sys.argv[2]
  sys.exit(3)

refOut.write( "file0 A file0-A start end <0> " )
for ph in refFile:
  refOut.write( ph )
refOut.write( "\n" )

refOut.close()
refFile.close()

for line in hypFile:
  for ph in line.split():
    hypOut.write( "file0 A start duration "+ph+"\n" )

hypOut.close()
hypFile.close()

args = [ '-r', sys.argv[1]+'.stm', 'stm', '-h', sys.argv[2]+'.ctm', 'ctm' ]

cmd = 'sclite '
for arg in args:
  cmd += arg+' '

os.system( cmd )

