
# Continuous state hidden Markov model #

Code to replicate the results published in the CSL paper.

The source code should compile with a standard C++ compiler, for example `g++`.  There is a suitable `Makefile` in the `src/` directory.  After compilation, we can run a simple test

```
./src/makePhons.x > phonemes.dat
./src/makeSynth.x 100
./src/decode.x phonemes.dat synth.dat > decode.dat
./py/score.py truth.dat decode.dat
```

The final scoring step uses the standard `sclite` package.
